---
title: Next Generation Studio
menu: Home
body_classes: ""
onpage_menu: false

content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _services
---
