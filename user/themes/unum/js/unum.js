var isTouch = window.DocumentTouch && document instanceof DocumentTouch;

jQuery(document).ready(function($){

    // Infinity Symbol Animation
    var infinityFull = KUTE.fromTo('#infinity',{draw:'0% 100%'}, {draw:'0% 0%'}, {easing: 'easingSinusoidalInOut'}, {duration: 2500});
    var infinityEmpty = KUTE.fromTo('#infinity',{draw:'100% 100%'}, {draw:'0% 100%'}, {easing: 'easingSinusoidalInOut'}, {duration: 1500});

    infinityFull.start();
    infinityFull.chain(infinityEmpty);
    infinityEmpty.chain(infinityFull);
});


// SmoothState
$(function() {
  $('#main').smoothState();
});
