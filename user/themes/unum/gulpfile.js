var postcss = require('gulp-postcss');
var gulp = require('gulp');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var cssnano = require('gulp-cssnano');
var sass = require('gulp-sass');
var size = require('gulp-size');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var concatCSS = require('gulp-concat-css');
// var immutablecss = require('immutable-css');
// var bemlinter = require('postcss-bem-linter');
// var customselectors = require('postcss-custom-selectors');
// var nested = require('postcss-nested');
// var nestedvars = require('postcss-nested-vars');
// var pseudoelements = require('postcss-pseudoelements');
// var responsiveimages = require('postcss-responsive-images');
// var simplevars = require('postcss-simple-vars');


gulp.task('css', function() {
  var s = size();
  return gulp.src('./scss/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.init())
    .pipe(postcss([autoprefixer({
        browsers: ['last 2 versions']
    })]))
    // .pipe(cssnano())
    .pipe(rename('styles.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(s)
    .pipe(gulp.dest('./css'))
    .pipe(notify({
        onLast: true,
        message: function () {
            return 'Done! ' + 'Total size ' + s.prettySize;
        }
    }));
});

gulp.task('watch', function() {
  gulp.watch('scss/**/*.scss', ['css']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch'], function() {

});
